%%
%% model generated rs-FC matrices convert to absolute matrices, thresholded with various correlation values, binarize matrices, and compute graph-theoretic measures 
%% using Brain Connectivity Toolbox (BCT) 
% input: model generated rs-FCs at various concentrations of GABA-glutamate 
% thr_range = various threshold values
% glut_conc_range = various concentrations of glutamate
% gaba_conc_range = various concentrations of GABA
% note: compute graph-based measures using functions such as 'modularity','clstr_coeff','char_path_len',and 'glob_eff' of BCT
% ouput: graph theoretical measures (clustering coefficient,characteristic path length, global efficiency, and modularity)  
% across various concentrations of GABA-glutamate at various threshold values 
%% 
clc;
clear;
addpath BCT;
glut_conc_range = 0.1:0.1:15; % concentration of glutamate, ranging from 0.1 to 15 mmol with increment of 0.1
gaba_conc_range = 0.1:0.1:15; % concentration of glutamate, ranging from 0.1 to 15 mmol with increment of 0.1
thr_range = 0.1:0.01:0.51;    % range of threshold values
G = 0.69;                     % selected global coupling factor 
%%
clstr_coeff = zeros(length(glut_conc_range),length(gaba_conc_range));    % initialization of clustering coefficient
char_path_len = zeros(length(glut_conc_range),length(gaba_conc_range));  % initialization of characteristic path length
glob_eff = zeros(length(glut_conc_range),length(gaba_conc_range));       % initialization of global efficiency
modularity = zeros(length(glut_conc_range),length(gaba_conc_range));     % initialization of modularity
%%
poolobj = parpool(4);
for k = 1:length(thr_range)
  for i = 1:length(glut_conc_range)
    glut_conc = glut_conc_range(i);
    parfor j = 1:length(gaba_conc_range)
      gaba_conc = gaba_conc_range(j);
      % load model generated rs-FC matrices
      savePath = ['/../../G=',num2str(G),'_glutconc=',num2str(glut_conc),'_gabaconc=',num2str(gaba_conc),'.mat'];
      fcSim = load(savePath,'fcSimBold');
      fcSim.fcSimBold = fcSim.fcSimBold;                                % convert to absolute matrix 
      fcSimBoldThr = threshold_proportional(fcSim.fcSimBold, thr_range(k));   % thresholded with various correlation values
      fcSimBoldThrBin = weight_conversion(fcSimBoldThr, 'binarize');          % binarize matrices  
      C = clustering_coef_bu(fcSimBoldThrBin);                              
      clstr_coeff(i,j) = mean(C);                                             % compute clustering coefficient
      D = distance_bin(fcSimBoldThrBin);
      [lambda,geff] = charpath(D,0,0);
      char_path_len(i,j) = lambda;                                            % compute characteristic path length 
      glob_eff(i,j) = geff;                                                   % compute global efficiency
      [Ci,Q] = modularity_und(fcSimBoldThrBin);
      modularity(i,j) = Q;                                                    % compute modularity
    end
  end
  save(['/../../graph_measures_thr=',num2str(thr_range(k)),'_bin_.mat'],'clstr_coeff','char_path_len','glob_eff','modularity'); % addpath to save
end
delete(poolobj);
%%