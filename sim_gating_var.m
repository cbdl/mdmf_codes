function [s,t] = sim_gating_var(gatingType,duration,dt,varargin)
  if(strcmpi(gatingType,'Destexhe_Simple_Kinetic'))
  %% Computes gating variable kinetic model as proposed in: Destexhe, Alain, Zachary F. Mainen, and Terrence J. Sejnowski. "An efficient method 
  %% for computing synaptic conductances based on a kinetic model of receptor binding."  Neural computation 6.1 (1994): 14-18.
    %fprintf('\nUsing Destexhe kinetic model for simulating the synaptic gating variable dynamics'\n);
    PreSynFRModel = varargin{1}; % Pre-synaptic firing rate Model(regular/poisson)
    firingRate = varargin{2}; % Pre-synaptic firing Rate
    alpha = varargin{3}; % Alpha
    beta = varargin{4}; % Beta
    Cmax = varargin{5}; % Max. Neurotransmitter concentration
    [spikeTrain,t] = gen_spikes(firingRate,duration,dt,PreSynFRModel);
    s = zeros(length(t),1);
    for i = 2:length(t)
      s(i) = s(i-1) + dt*(alpha*Cmax*spikeTrain(i-1)*(1-s(i-1)) - beta*s(i-1));
    end
  elseif(strcmpi(gatingType,'Brunel_Wang_2001'))
  %% Computes gating variable following the kinetic model given Brunel, Nicolas, and Xiao-Jing Wang. "Effects of neuromodulation in a cortical network model 
  %% of object working memory dominated by recurrent inhibition." Journal of computational neuroscience 11.1 (2001): 63-85. Equations are modified to accomodate for
  %% neurotransmitter concentration
    %fprintf('\nUsing Brunel and Wang, 2001 kinetic model for simulating the synaptic gating variable dynamics\n');
    PreSynFRModel = varargin{1}; % Pre-synaptic firing rate Model(regular/poisson)
    firingRate = varargin{2}; % Pre-synaptic firing Rate
    %fprintf('size of firingRate: %d x %d',size(firingRate,1),size(firingRate,2))
    synType = varargin{3}; % Type of synapse (AMPA/GABA/NMDA)
    [spikeTrain,t] = gen_spikes(firingRate,duration,dt,PreSynFRModel);
    s = zeros(length(t),1);
    if(strcmpi(synType,'AMPA') || strcmpi(synType,'GABA'))
      tau = varargin{4}; % Synaptic time constant
      Cmax = varargin{5}; % Max. Neurotransmitter concentration
      for i = 2:length(t)
        s(i) = s(i-1) + dt*(-s(i-1)/tau + Cmax*spikeTrain(i-1));                       
      end
    elseif(strcmpi(synType,'NMDA'))
      taur = varargin{4}; % Rise time constant
      taud = varargin{5}; % Decay time constant
      Cmax = varargin{6}; % Max. Neurotransmitter concentration
      alpha = varargin{7};
      x = zeros(length(t),1);
      for i = 2:length(t)
        x(i) = x(i-1) + dt*(-x(i-1)/taur + Cmax*spikeTrain(i-1));
        s(i) = s(i-1) + dt*(-s(i-1)/taud + (1-s(i-1))*alpha*x(i-1));                       
      end
    end
  else
    fprintf('\nUnkown gating type:%s\n',gatingType);
    s=[];
    t=[];
  end
end