%%
%% this is a source code, generates simulated rs-FCs at various concentrations of neurotransmitters 
% input:
% glut_conc_range = glutamate concentrations
% gaba_conc_range = GABA concentrations
% nAreas = number of brain areas (68-parcellated cortex)
% avgSC68 = structural connectivity matrix
% note: its input function is MDMFalphan_beta68.m
% output:
% generates simulated rs-FCs at various concentrations of GABA-glutamate 
%%
clc;
clear;
saveResults = true;
simTime = 9*60*1000;           % total simulation time     
nAreas = 68;                   % number of brain areas,determined based on structural connectivity matrix
glut_conc_range = 0.1:0.1:15;  % simulated at various concentration of glutamate, ranging from 0.1 to 15 mmol with increment of 0.1  
gaba_conc_range = 0.1:0.1:15;  % simulated at various concentration of GABA, ranging from 0.1 to 15 mmol with increment of 0.1
G_range = 0.69;                % selected global coupling factor
sigma = 0.001; 
dt = 0.1;
doPlot = false;
initVals.SE = 0.001*ones(nAreas,1);
initVals.SI = 0.001*ones(nAreas,1);
initVals.J = ones(nAreas,1);
poolobj=parpool(2); 
%%
% load structural connectivity matrix, 68-parcellated cortex (avg. of 40 subjects)
scPath = load('avgSC68');
SC = scPath.avgSC40;
% setting self-connections to 0 
SC(1:nAreas+1:nAreas*nAreas)=0;
%%
for i = 1:size(G_range,2)
    G = G_range(i);
    for j = 1:size(glut_conc_range,2)
               T_Glu = glut_conc_range(j);
        parfor k = 1:size(gaba_conc_range,2) 
                   T_Gaba = gaba_conc_range(k); 
                 savePath = ['/home/lab/Documents/GitHub_MDMF/testmdmf_68/G=',num2str(G),'_glutconc=',num2str(T_Glu),'_gabaconc=',num2str(T_Gaba),'.mat']; % 'add path' to save results
            if ~exist(savePath,'file')
              tstart = tic;
               fprintf('Starting simulation for G = %.2f, glut_conc = %.2f, gaba_conc = %.2f\n',G,T_Glu,T_Gaba); 
               MDMFalphan_beta68(SC,G,T_Glu,T_Gaba,sigma,dt,saveResults,savePath,initVals,simTime,nAreas);
               telapsed = toc(tstart);
              fprintf('Time elapsed: %.2f minutes\n',telapsed/60.);
            end
        end
    end
end
delete(poolobj)
%%