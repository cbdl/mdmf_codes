function [simOut] = MDMFalphan_beta150ts(SC,G,T_Glu,T_Gaba,sigma,dt,saveResults,savePath,initVals,simTime,nAreas)
%% This code simulates synaptic activity using MDMF equations Naskar et al and computes BOLD time series (Friston BALLOON MODEL)
%% Inputs
% G = Global coupling factor
% T_Glu =  Concentration of glutamate in mmol
% T_Gaba = Concentration of GABA in mmol
% sigma =  std. of noise used for simulating synaptic gating variable
% dt = Stepsize for Euler integration
% saveResults =  FileName to save results
% savePath = Path to save results
% initVals = Initial values for gating variables SE and SI
% simTime = total simulation time
% nAreas = number of brain areas (150-parcellated cortex)
%% Outputs
% simOut.maxfr_E = Maximum firing rate of excitatory population
% simOut.maxfr_I = Maximum firing rate of inhibitory population
% simOut.BOLDActDS = BOLD time-series generated across 150 brain areas 

%% Model parameters
JNMDA = 0.15;        % scaling factor to get low spontaneous activity for isolated node
I0 = 0.382;         % input current to each node
WE = 1.0;           % scaling of external input current to excitatory population
WI = 0.7;          % scaling of external input current to inhibitory population
wplus = 1.4;        % weight for recurrent self-excitation in each excitatory population
gamma = 1.0;        % learning rate
alphaE = 0.072;     % forward rate constant for NMDA gating
betaE = 0.0066;     % backward rate constant for NMDA gating
alphaI = 0.53;      % forward rate constant for GABA gating
betaI = 0.18;       % backward rate constant for GABA gating
rho = 3;            % target-firing rate of the excitatory population is maintained at the 3 Hz

%% Parameters controlling numerics
dtt   = 1e-3;         % sampling rate of simulated neuronal activity (seconds)
ds    = 100;          % BOLD downsampling rate
tSpan = 0:dt:simTime; 

%% Initializing Variables
S_E = zeros(simTime,nAreas);        % initialization of excitatory synaptic gating variable
S_I = zeros(simTime,nAreas);        % initialization of inhibitory synaptic gating variable
J_IE = zeros(simTime,nAreas);       
simOut.maxfr_E = zeros(nAreas,1);   % initialization of mean firing rate for excitatory population of each brain area
simOut.maxfr_I = zeros(nAreas,1);   % initialization of mean firing rate for inhibitory population of each brain area


disp('simulating synaptic gating dynamics...')
SE = initVals.SE;  % initialised values of excitatory synaptic gating variable
SI = initVals.SI;  % initialised values of inhibitory synaptic gating variable
J = initVals.J;    % initialised value of learning rate
nn=1;
j=0;

for i=2:1:length(tSpan)
    IE = WE*I0 + wplus*JNMDA*SE + G*JNMDA*SC*SE-J.*SI;          % input current to the excitatory population
    II = WI*I0 + JNMDA*SE - SI;                                 % input current to the inhibitory population
    rE = phie(IE);                                              % firing rate of excitatory population in brain area
    rI = phii(II);                                              % firing rate of inhibitory population in brain area
    SEdot = - betaE*SE+(alphaE./1000.0)*T_Glu*(1-SE).*rE;       % kinetic equation represents glutamate binding with recepor (NMDA)
    nun = randn(nAreas,1);
    SE=SE+dt*SEdot+sqrt(dt)*sigma*nun;                          % noise term added (uncorrected standard Gaussian noise with the noise amplitude (sigma) in each brain area)
    SE(SE>1) = 1;
    SE(SE<0) = 0;
    nug = randn(nAreas,1);
    SIdot = -betaI*SI+(alphaI./1000.0)*T_Gaba*(1-SI).*rI;       % kinetic equation represents GABA binding with inhibitory receptor
    SI=SI+dt*SIdot+sqrt(dt)*sigma*nug;                          % noise term, uncorrected standard Gaussian noise with the noise amplitude (sigma) in each brain area)
    SI(SI>1) = 1;
    SI(SI<0) = 0;
    Jdot = gamma*(rI./1000.0).*((rE-rho)./1000.0);              % J, synaptic weight and its dynamics clamps firing rate of the excitatory population
    J = J + dt*Jdot;
    j=j+1;
    if j==1/dt

        S_E(nn,:)=SE';
        S_I(nn,:)=SI';
        
        J_IE(nn,:) = J';
        if(nn>60*1000)
            simOut.maxfr_E = max(simOut.maxfr_E,rE);
            simOut.maxfr_I = max(simOut.maxfr_I,rI);
        end
        nn=nn+1;
        j=0;
    end
    
end

disp('simulation completed and synaptic activity computed')
nn = nn-1;
%% BOLD empirical calculation using Hemodynamic model
% Friston BALLOON MODEL
disp('computing BOLD signals using synaptic activity...')
T = nn*dtt - 60; % Total time in seconds ignoring the first 60 seconds to allow for initial transients

B = BOLD(T,S_E(60001:end,1)); % B=BOLD activity, bf=Fourier transform, f=frequency range)
BOLDAct = zeros(length(B),nAreas);
BOLDAct(:,1) = B;

for areaNo=2:nAreas
    B = BOLD(T,S_E(60001:nn,areaNo));
    BOLDAct(:,areaNo) = B;
end
disp('BOLD Signals computed')

 simOut.BOLDActDS=BOLDAct(1:ds:end,:);
 %simOut.fcSimBold = corrcoef(BOLDActDS);
 
if(saveResults)
    %fprintf('Saving simulation results for G=%.2f T_Glu=%.2f T_Gaba=%0.2f\n',G,T_Glu,T_Gaba);
    save(savePath,'-v7.3','-struct','simOut');
    fprintf('Save complete\n')
end
end