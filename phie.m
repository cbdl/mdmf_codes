function rE=phie(x)
% excitatory gating variables used in MDMF model
dE=0.16;
bE=125.;
aE=310.;
y=aE*x-bE;
rE = y./(1-exp(-dE*y));
end