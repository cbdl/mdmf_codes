% Calculate filtered signal for a given HighPass, LowPass, fs(sampling freq)
% Input:
% data: NTimes * NROIs/NSensors (considering BOLD signal of 68 or 150 nodes)
% HighPass: High Pass freq 
% LowPass: Low Pass freq
% fs: sampling frequency(1/TR in case of fMRI)
% Note: Consider declaring bpFilt outside the function for faster execution
% Output: band-Pass filtered BOLD signal  
clc;
clear;
HighPass = 0.06;
LowPass = 0.03;
fs = 1/2; 
glut_conc_range = 0.1:0.1:15;
gaba_conc_range = 0.1:0.1:15;
G_range = 0.69;

for ij  = 1:length(G_range)
            G = G_range(ij);
            for im = 1:length(glut_conc_range)
                glut_conc  = glut_conc_range(im);
                 for m = 1:length(gaba_conc_range)
                  gaba_conc = gaba_conc_range(m);
                  % load model generated BOLD signal (BOLD signal of 68 or 150 nodes) 
                    savePath = ['/../../G=',num2str(G),'_glutconc=',num2str(glut_conc),'_gabaconc=',num2str(gaba_conc),'.mat']; % add path
                    BOLDActDS1 = load(savePath,'BOLDActDS','maxfr_E');
                    BOLDActTs = BOLDActDS1.BOLDActDS;
                    mxfiring = BOLDActDS1.maxfr_E;
                    BP_simulatedMtrx = BPmeta(BOLDActTs,HighPass,LowPass,fs);
                    % path to save band-Pass filtered BOLD signal
                    save_BP = ['/../../G=',num2str(G),'_glutconc=',num2str(glut_conc),'_gabaconc=',num2str(gaba_conc),'.mat'];   
                    save(save_BP,'BP_simulatedMtrx','mxfiring');
                 end
            end
 end
 

