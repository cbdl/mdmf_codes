function [spikeTrain,t] = gen_spikes(firingRate,duration,dt,type)
  % Generates a spike Train with frequency 'firingRate(Hz)' for 'duration(s)' time at a time resolution of 'dt(ms)'
  t = 0:dt:(duration*1000);
  spikeTrain = zeros(length(t),1);
  refractory = false;
  refPeriod = 1;
  refJump = refPeriod/dt;
  if(strcmpi(type,'regular'))
    timePeriod = (1000.0./firingRate);
    tpatdt = round(timePeriod/dt); % timePeriod at time resolution of dt
    spikeNo = 1;
    startIdx = spikeNo*tpatdt + 1;
    endIdx = startIdx + 1/dt;
    while(startIdx <= length(t))
      endIdx = startIdx + 1/dt;
      if(endIdx <= length(t))
        spikeTrain(startIdx:endIdx) = 1;
      else
        spikeTrain(startIdx:end) = 1;
      end
      spikeNo = spikeNo + 1;
      startIdx = (spikeNo*tpatdt) + 1;
    end
  elseif(strcmpi(type,'poisson'))
    startIdx = 1;
    while(startIdx <= length(t))
      samp = rand();
      if(samp <= (firingRate/1000.0)*dt)
        endIdx = startIdx + 1/dt - 1;
        spikeTrain(startIdx:endIdx) = 1;
        startIdx = endIdx;
      else
        spikeTrain(startIdx) = 0;
        startIdx = startIdx + 1;
      end
    end
  end
end