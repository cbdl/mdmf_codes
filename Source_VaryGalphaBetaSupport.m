%%
%% this is a source code, varied global coupling fcator (G) at fixed concentration of glutamate and GABA, solved the system of equations (3)−(9) numerically
% input: G_range = range of global coupling factor
% note: input function is VaryGalphan_beta.m 
% various global coupling factors are used at fixed concentration of GABA (1.82 mmol) & glutamate (7.46 mmol) 
% output: simulated-FCs, firing rate of excitatory and inhibitory population are generated considering various G values 
%%
G_range = 0:0.01:1.5; % global coupling factors, ranging from 0 to 1.5 with the increment 0.01  
simTime = 9*60*1000;
nAreas = 68;
saveResults = true;
doPlot = false;
initVals.SE = 0.001*ones(nAreas,1);
initVals.SI = 0.001*ones(nAreas,1);
initVals.J = ones(nAreas,1); 
%%
poolobj=parpool(4); 
disp('it started')
parfor jm = 1:1:length(G_range)
             G = G_range(jm);
             % save simulated-FCs, firing rate of excitatory and inhibitory population generated with different G values 
             savePath = ['/../../G=',num2str(G),'.mat']; % add path
             %savePath = ['G=',num2str(G),'.mat'];
             if ~exist(savePath,'file')
              tstart = tic;
              VaryGalphan_beta(saveResults,savePath,initVals,simTime,G);
              telapsed = toc(tstart);
              fprintf('Time elapsed: %.2f minutes\n',telapsed/60.);
             end
end
            
delete(poolobj)
%%