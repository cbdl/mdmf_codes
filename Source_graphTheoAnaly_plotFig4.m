  %%
  %% plot changes in graph theoretical measures of simulated rs-FCs (following network construction) as function of GABA-glutamate concentrations   
  % input: graph theoretical measures (thresholded at 0.25) 
  % glut_conc_range = various concentration of glutamate
  % gaba_conc_range = various concentration of GABA
  % output: 
  % plot graph theoretical measures as function of GABA-glutamate concentrations, Fig.4A = modularity, Fig.4B =  clustering coefficient,
  % plot graph theoretical measures at discrete concentration of glutamate with various concentrations of GABA, Fig.4C = modularity, Fig.4D =  clustering coefficient
  %%
  clc; 
  clear;
  % load FC matrix, thresholded with 0.25
  graph_measures = load('/../../graph_measures_thr=0.25_bin_.mat'); % add path
  
  %%
  glut_conc = 0.1:0.1:15;                                    % various concentration of glutamate, ranging from 0.1 to 15 mmol with increment of 0.1 
  gaba_conc = 0.1:0.1:15;                                    % various concentration of GABA, ranging from 0.1 to 15 mmol with increment of 0.1 
  modularity =  graph_measures.modularity;                   % modularity
  cluster_coefficient = graph_measures.clstr_coeff;          % clustering coefficient
  %% 
  figure();
  %subplot(1,2,1);
  contourf(gaba_conc,glut_conc,modularity);
  colorbar();
  set(gca,'YDir','normal')
  xlabel('GABA Conc.');
  ylabel('Glutamate Conc.');
  title('Modularity','FontWeight','normal');
  
  figure();
  %subplot(1,2,1);
  contourf(gaba_conc,glut_conc,cluster_coefficient);
  colorbar();
  set(gca,'YDir','normal')
  xlabel('GABA Conc.');
  ylabel('Glutamate Conc.');
  title('Clustering Coefficient','FontWeight','normal');
  
      
  %%
  glut_conc_range = 7:2:9;      % various concentration of glutamate, ranging from 7 to 9 mmol with increment of 2
  gaba_conc_range = 0.1:0.1:15; % various concentration of GABA, ranging from 0.1 to 15 mmol with increment of 0.1 
 
  for i = 1:length(glut_conc_range)
    subplot(1,2,1);
    plot(gaba_conc_range,graph_measures.modularity(10*glut_conc_range(i),1:length(gaba_conc_range)));
    hold on;
    xlabel('GABA Conc.','FontWeight','normal');
    ylabel('Modularity','FontWeight','normal');
    legend('Glut. Conc=7','Glut. Conc=9');
    
    subplot(1,2,2);
    plot(gaba_conc_range,graph_measures.clstr_coeff(10*glut_conc_range(i),1:length(gaba_conc_range)));
    hold on;
    xlabel('GABA Conc.','FontWeight','normal');
    ylabel('Clustering Coefficient','FontWeight','normal');
    legend('Glut. Conc=7','Glut. Conc=9');
         
     
  end
 %% 
 
  
  
  
  
 
  