clc; 
clear;
%% this code is used to generate figures, (Fig.2A-B; GABA- or NMDA- synaptic gating as a function of population mean firing rate which approximate the model proposed by Destexhe (1994b). 
%% Computes time averaged gating variable for various pre-synaptic firing rate by numerically solving kinetic gating scheme proposed in:
%% Destexhe, Alain, Zachary F. Mainen, and Terrence J. Sejnowski. "An efficient method for computing synaptic conductances based on a kinetic model of receptor binding." 
%% Neural computation 6.1 (1994): 14-18.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ChannelRate = [1.1 0.19;0.072 0.0066;0.53 0.18]; % column 1: forward rate(close to open state) constant, column 2 : backward rate(open to close state) constant for ligand gated channels
NTCmax = [1 1 1]; % max. neurotransmitter concentration
duration = 5;
dt = 0.01;
firingRate = 1:1:300;
PreSynFRModel = 'poisson';
sAvg = zeros(length(firingRate),1);
gatingType = 'Destexhe_Simple_Kinetic';
for j = 1:size(ChannelRate,1)
  switch(j)
  case 1
    alpha = ChannelRate(j,1);
    beta = ChannelRate(j,2);
    Cmax = NTCmax(j);
    for i = 1:length(firingRate)
      [s,t] = sim_gating_var(gatingType,duration,dt,PreSynFRModel,firingRate(i),alpha,beta,Cmax);
%       sAvg(i) = mean(s);
        sAvg(i) = mean(s(200:end));
    end
    save('\..\..\avgGatingAMPADestexhepoisson.mat','sAvg','duration','dt','Cmax','alpha','beta','firingRate'); % add path to save results
    figure();
    axAMPA = subplot(1,1,1);
    plot(axAMPA,firingRate,sAvg,'Color',[0 0.4470 0.7410],'Marker','o','MarkerSize',3,'LineStyle','none');
    hold on;
    title('AMPA Synaptic Gating');
    xlabel('Firing Rate','FontWeight','bold');
    ylabel('Average Gating','FontWeight','bold');
%    hold on;
  case 2
    alpha = ChannelRate(j,1);
    beta = ChannelRate(j,2);
    Cmax = NTCmax(j);
    for i = 1:length(firingRate)
      [s,t] = sim_gating_var(gatingType,duration,dt,PreSynFRModel,firingRate(i),alpha,beta,Cmax);
%       sAvg(i) = mean(s);
        sAvg(i) = mean(s(200:end));
    end
    save('\..\..\avgGatingNMDADestexhepoisson.mat','sAvg','duration','dt','Cmax','alpha','beta','firingRate'); % add path to save result
    figure();
    axNMDA = subplot(1,1,1);
    plot(axNMDA,firingRate,sAvg,'Color',[0.8500 0.3250 0.0980],'Marker','o','MarkerSize',3,'LineStyle','none');
    hold on;
    title('NMDA Synaptic Gating');
    xlabel('Firing Rate','FontWeight','bold');
    ylabel('Average Gating','FontWeight','bold');
  case 3
    alpha = ChannelRate(j,1);
    beta = ChannelRate(j,2);
    Cmax = NTCmax(j);
    for i = 1:length(firingRate)
      [s,t] = sim_gating_var(gatingType,duration,dt,PreSynFRModel,firingRate(i),alpha,beta,Cmax);
%       sAvg(i) = mean(s);
        sAvg(i) = mean(s(200:end));
    end
    save('\..\..\avgGatingGABADestexhepoisson.mat','sAvg','duration','dt','Cmax','alpha','beta','firingRate'); % add path
    figure();
    axGABA = subplot(1,1,1);
    plot(axGABA,firingRate,sAvg,'Color',[0.9290 0.6940 0.1250],'Marker','o','MarkerSize',3,'LineStyle','none');
    hold on;
    title('GABA Synaptic Gating');
    xlabel('Firing Rate','FontWeight','bold');
    ylabel('Average Gating','FontWeight','bold');
  otherwise
    fprintf('Unknown case %d\n',j);
  end  
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Computes average gating variable from our model

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%firingRate = 0.1:0.1:300;
Cglut = NTCmax(2);
Cgaba = NTCmax(3);
etaE = 0.68;
etaI =0.6 ;

alphaE = 0.072;  % forward rate constant for NMDA gating
betaE = 0.0066;  % backward rate constant for NMDA gating
alphaI = 0.53;   % forward rate constant for GABA gating
betaI = 0.18;    % backward rate constant for GABA gating

r = firingRate ./ 1000; % converting to milli seconds
sAvgNMDA = (etaE*alphaE*Cglut*r)./(betaE + etaE*alphaE*Cglut*r);
sAvgGABA = (etaI*alphaI*Cgaba*r)./(betaI + etaI*alphaI*Cgaba*r);

%figure();
plot(axNMDA,firingRate,sAvgNMDA,'Color',[0.8500 0.3250 0.0980],'Marker','x','LineStyle','none','MarkerSize',3);
legend(axNMDA,'Destexhe','Our model')
%hold on;
plot(axGABA,firingRate,sAvgGABA,'Color',[0.9290 0.6940 0.1250],'Marker','x','LineStyle','none','MarkerSize',3);
legend(axGABA,'Destexhe','Our model')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%