%Calculate metastability(signals need to be Bandpassed prior to this)
%Input:
%BpSignal is NTime * NSensors(otherwise transpose)
%Output:
%Metastability Index

function m = MetaStab(BpSignal)
PhaseSignal = angle(hilbert(BpSignal)); %NTime * NSensors
PhaseSignal = exp(1i*PhaseSignal);% NSensors * NTime (earlier inverse)
order_parameter = mean(PhaseSignal);
m = std(abs(order_parameter)); %across
end

