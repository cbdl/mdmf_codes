  %%
  %% plot changes in graph theoretical measures of simulated rs-FCs (following network construction) as function of GABA-glutamate concentrations   
  % input: graph theoretical measures (thresholded at 0.25) 
  % glut_conc_range = various concentration of glutamate
  % gaba_conc_range = various concentration of GABA
  % output: 
  % plot graph theoretical measures as function of GABA-glutamate concentrations, Fig.5A = characteristic path length, Fig.5B =  global efficiency
  % plot graph theoretical measures at discrete concentration of glutamate with various concentrations of GABA, Fig.5C = characteristic path length, Fig.5D = global efficiency
  %%
  clc; 
  clear;
  % load FC matrix, thresholded with 0.25
  graph_measures = load('/../../graph_measures_thr=0.25_bin_.mat'); % add path

  %%
  glut_conc = 0.1:0.1:15;                                    % various concentration of glutamate, ranging from 0.1 to 15 mmol with increment of 0.1 
  gaba_conc = 0.1:0.1:15;                                    % various concentration of GABA, ranging from 0.1 to 15 mmol with increment of 0.1 
  characteristic_pathlength = graph_measures.char_path_len;  % characteristic path length
  global_efficiency = graph_measures.glob_eff;               % global efficiency
  %% 
      
  figure();
  contourf(gaba_conc,glut_conc,characteristic_pathlength);
  colorbar();
  set(gca,'YDir','normal')
  xlabel('GABA Conc.');
  ylabel('Glutamate Conc.');
  title('Characteristic Path Length','FontWeight','normal');
  
  figure();
  contourf(gaba_conc,glut_conc,global_efficiency);
  colorbar();
  set(gca,'YDir','normal')
  xlabel('GABA Conc.');
  ylabel('Glutamate Conc.');
  title('Global Efficiency','FontWeight','normal');
  
   
  %%
  glut_conc_range = 7:2:9;      % various concentration of glutamate, ranging from 7 to 9 mmol with increment of 2
  gaba_conc_range = 0.1:0.1:15; % various concentration of GABA, ranging from 0.1 to 15 mmol with increment of 0.1 
  %%    
  for i = 1:length(glut_conc_range)
       
       
    subplot(1,2,1);
    plot(gaba_conc_range,graph_measures.char_path_len(10*glut_conc_range(i),1:length(gaba_conc_range)));
    hold on;
    xlabel('GABA Conc.','FontWeight','normal');
    ylabel('Characteristic Path Length','FontWeight','normal');
    legend('Glut. Conc=7','Glut. Conc=9');
    
    subplot(1,2,2);
    plot(gaba_conc_range,graph_measures.glob_eff(10*glut_conc_range(i),1:length(gaba_conc_range)));
    hold on;
    xlabel('GABA Conc.','FontWeight','normal');
    ylabel('Global efficiency','FontWeight','normal');
    legend('Glut. Conc=7','Glut. Conc=9');
    
  end
 %% 
 
  
  
  
  
 
  