  %%
  %% this code is used to generate figures 
  % input: avgFC68.mat = empirical rs-FC 
  % G_range = range of global coupling factor
  % output:
  % Fig.2C, plot FC distance as a function of G
  % Fig.2D, plot average firing rate of excitatory population as a function of G
  % Fig.2E, plot average firing rate of inhibitory population as a function of G
  % Fig.2F, plot firing rate of excitatory population across brain areas at G = 0.69, bar(maxfr_E,'b');
  % Fig.2G, plot firing rate of inhibitory population across brain areas at G = 0.69, bar(maxfr_E,'b');
  %%
  clc;
  clear;
  %% load empirical functional connectivity matrix, 68-parcellated cortex (avg. of 40 subjects)
  fcPath = load('avgFC68');
  avgFC = fcPath.rs_FC;
  fcEmp = avgFC;
  %%
  G_range = 0:0.01:1.5; 
  globMaxFR_E =  zeros(length(G_range),1)';
  globMaxFR_I =  zeros(length(G_range),1)';
  fcDists = zeros(length(G_range),1)';
  %%
  for i = 1:size(G_range,2)
      G = G_range(i);
              % load simulated-FCs, firing rate of excitatory and inhibitory population generated with different G values 
              savePath = ['/../../G=',num2str(G),'.mat']; 
              if exist(savePath,'file')
                load(savePath,'fcSimBold','maxfr_E','maxfr_I'); 
                %fcCorrs(i) = find_corr(fcSimBold,fcEmp);
                fcDists(i) = fc_distance(fcSimBold,fcEmp,[]);
                globMaxFR_E(i) = mean(maxfr_E);
                globMaxFR_I(i) = mean(maxfr_I);
               
              end
  end

%%
  
  subplot(1,3,1);

  scatter(G_range,globMaxFR_E,'filled','r');
  xlabel('G (global coupling strength)','FontWeight','bold');
  ylim([0 60]);
  ylabel('Firing rate (excitatory population)','FontWeight','bold');
  
  subplot(1,3,2);
  scatter(G_range,globMaxFR_I,'filled','r');
  xlabel('G (global coupling strength)','FontWeight','bold');
  ylim([6 18]);
  ylabel('Firing rate (inhibitory population)','FontWeight','bold');
    
  subplot(1,3,3);
  scatter(G_range,fcDists,'filled','r');
  xlabel('G (global coupling strength)','FontWeight','bold');
  ylabel('FC distance','FontWeight','bold');
  
  %% select G = 0.69 and the corresponding firing rate, plot firing rate of excitatory and inhibitory population across brain areas 
  % pathG = load('G=0.69.mat');
  % maxE = pathG.maxfr_E;
  % maxI = pathG.maxfr_I;
  % figure(); bar(maxE,'b');
  % figure(); bar(maxI,'b');
  %%
  
  
  
  
  
  