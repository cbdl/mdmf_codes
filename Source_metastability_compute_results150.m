%%
%% compute metastability of model generated band-pass filtered BOLD signal and plot metastability as function of concentrations of neurotransmitters    
% input: band-pass filtered BOLD signal
% glut_conc_range = glutamate concentrations
% gaba_conc_range = GABA concentrations
% note: 'MetaStab' function to compute metastability of BOLD signal  
% output: metastability of BOLD signal at various concentrations of GABA-glutamate 
% Fig.3D, plot metastability as function of GABA-glutamate concentrations
%%
clear; 
clc;
glut_conc_range = 0.1:0.1:15;               % concentration of glutamate, ranging from 0.1 to 15 mmol with increment of 0.1 
gaba_conc_range = 0.1:0.1:15;               % concentration of GABA, ranging from 0.1 to 15 mmol with increment of 0.1
G_range = 0.69;                             % selected global coupling factor
maxfr = zeros(1,size(glut_conc_range,2));   % initialization of firing rate for excitatory population
metastb_Vls = zeros(150,150);               % initialization of metastability 
for ij  = 1:length(G_range)
            G = G_range(ij);
            for im = 1:length(glut_conc_range)
                glut_conc  = glut_conc_range(im);
                 for m = 1:length(gaba_conc_range)
                  gaba_conc = gaba_conc_range(m);
                       %load band-pass filtered BOLD signal
                       savePath = ['/../../G=',num2str(G),'_glutconc=',num2str(glut_conc),'_gabaconc=',num2str(gaba_conc),'.mat'];
                       load(savePath,'BP_simulatedMtrx','mxfiring');
                          maxfr = max(mxfiring);
                       if (maxfr >= 0.1) && (maxfr <= 100)
                        save_FC = ['/../../G=',num2str(G),'_glutconc=',num2str(glut_conc),'_gabaconc=',num2str(gaba_conc),'.mat'];
                        save(save_FC,'BP_simulatedMtrx','mxfiring');
                        BpSg_path = ['/../../G=',num2str(G),'_glutconc=',num2str(glut_conc),'_gabaconc=',num2str(gaba_conc),'.mat'];
                        BpSignal_1 = load(BpSg_path,'BP_simulatedMtrx');
                        BpSignal_2 =  BpSignal_1.BP_simulatedMtrx;
                        metastb_Vls(im,m) = MetaStab(BpSignal_2); % computing metastability
                         % add path to save metastability
                        save_metaVls = ['/../../G=',num2str(G),'_glutconc=',num2str(glut_conc),'_gabaconc=',num2str(gaba_conc),'.mat'];
                        save(save_metaVls,'metastb_Vls');
                       end
                                         
                 end 
            end
end
%%
% contour plot, metastability as a function of GABA-glutamate concentrations 
glut = 0.1:0.1:15;
gaba = 0.1:0.1:15;
G_range = 0.69;

for i  = 1:length(G_range)
            G = G_range(i);
            for k = 1:length(glut)
                glut_conc  = glut(k);
                 for m = 1:length(gaba)
                  gaba_conc = gaba(m);
                     save_metaVls = ['/../../G=',num2str(G),'_glutconc=',num2str(glut_conc),'_gabaconc=',num2str(gaba_conc),'.mat'];
                     if exist(save_metaVls,'file')
                     z = load(save_metaVls,'metastb_Vls');
                                        
                     end
                       
                  end
                      
                   
            end
end

zvalues = z.metastb_Vls;
figure();
contour3(gaba,glut,zvalues);
colormap(colorcube);
ylim([0 15]);
xlabel('GABA Conc. (mmol)','FontWeight','normal','FontSize',12);
ylabel('Glutamate Conc. (mmol)','FontWeight','normal','FontSize',12);
title('Metastability (150-parcellated cortex)','FontWeight','normal','FontSize',12); 
%%  
  
                    
                
    

