  
  %% Comparison of empirical rs-FC and simulated rs-FCs, this code is used to generate figures, Fig.6A, Fig.6B, Fig.6C, Fig.6D, Fig.6E & Fig.6F
  % input: 
  % simulated FC (glutamate: 12 & GABA: 1.5) = G=0.69_glutconc=12_gabaconc=1.5.mat 
  % simulated FC (glutamate: 7 & GABA: 1.5) = G=0.69_glutconc=7_gabaconc=1.5.mat 
  % simulated FC (glutamate: 3 & GABA: 1.5) = G=0.69_glutconc=3_gabaconc=1.5.mat
  % simulated DMN (glutamate: 12 & GABA: 1.5) = glt12gaba1point5.mat
  % simulated DMN (glutamate: 7 & GABA: 1.5) = glt7gaba1point5.mat
  % simulated DMN (glutamate: 3 & GABA: 1.5) = glt3gaba1point5.mat
  % Output:
  % Fig.6A = empirical rs-FC; Fig.6B = simulated rs-FC (12 mmol glutamate & 1.5 mmol GABA); Fig.6C = simulated rs-FC (7 mmol glutamate & 1.5 mmol GABA); Fig.6D = simulated rs-FC (3 mmol glutamate & 1.5 mmol GABA) 
  % Fig.6E, FC distance between empirical rs-FC and simulated rs-FC obtained at various glutamate concentrations including 12 mmol, 7 mmol or 3 mmol glutamate with GABA fixed to 1.5 mmol
  % Fig.6F, FC distance between empirical default mode network (DMN) and simulated DMN obtained at various glutamate concentrations including 12 mmol, 7 mmol or 3 mmol glutamate with GABA fixed to 1.5 mmol
  clc; 
  clear;
  empFCpath = load('empFC');
  empiricalFC = empFCpath.empFC;
  %%
  glt12gabaOnepointFivePath = load('glt12gaba1point5');
  glt12gabaOnepointFive = glt12gabaOnepointFivePath.glt12gaba1point5;
  %%
  glt7gabaOnepointFivePath = load('glt7gaba1point5');
  glt7gabaOnepointFive = glt7gabaOnepointFivePath.glt7gaba1point5;
  %%
  glt3gabaOnepointFivePath = load('glt3gaba1point5');
  glt3gabaOnepointFive = glt3gabaOnepointFivePath.glt3gaba1point5;
   %%
  fcDists1 = fc_distance(glt12gabaOnepointFive,empiricalFC,[])/7;
  fcDists2 = fc_distance(glt7gabaOnepointFive,empiricalFC,[])/7;
  fcDists3 = fc_distance(glt3gabaOnepointFive,empiricalFC,[])/7;
  %%
  figure();
  fcDistance = [fcDists1 fcDists2 fcDists3];
  bar(fcDistance,'k');
  xlabel('Glutamate Conc. (mmol)','FontWeight','normal','FontSize',12);
  ylabel('FC distance','FontWeight','normal','FontSize',12);
  title('DMN (GABA Conc. 1.5 mmol)','FontWeight','normal','FontSize',12); 
   %%   
  empFCallpath = load('avgFC40'); 
  empFCall = empFCallpath.rs_FC;
  figure(); imagesc(empFCall);
  %%
  glutconc12path = load('G=0.69_glutconc=12_gabaconc=1.5.mat');
  glutconc12 = glutconc12path.fcSimBold;
  figure(); imagesc(glutconc12);
  %%    
  glutconc7path = load('G=0.69_glutconc=7_gabaconc=1.5.mat');  
  glutconc7 = glutconc7path.fcSimBold; 
  figure(); imagesc(glutconc7);
  %%    
  glutconc3path = load('G=0.69_glutconc=3_gabaconc=1.5.mat');  
  glutconc3 = glutconc3path.fcSimBold; 
  figure(); imagesc(glutconc3);
  %%  
  fcDistsi1 = fc_distance(glutconc12,empFCall,[])/68;
  fcDistsi2 = fc_distance(glutconc7,empFCall,[])/68;
  fcDistsi3 = fc_distance(glutconc3,empFCall,[])/68; 
  %%
  figure();
  fcDistance = [fcDistsi1 fcDistsi2 fcDistsi3];
  bar(fcDistance,'k');
  xlabel('Glutamate Conc. (mmol)','FontWeight','normal','FontSize',12);
  ylabel('FC distance','FontWeight','normal','FontSize',12);
  title('rs-FC (GABA Conc. 1.5 mmol)','FontWeight','normal','FontSize',12); 
  %%
  
  