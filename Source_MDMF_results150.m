  %%
  %% this code is used to generate figures, plot mean firing rate and FC-distance, respectively as function of concentrations of neurotransmitters
  % input: avgFC150 = empirical rs-FC
  % glut_conc_range = glutamate concentrations
  % gaba_conc_range = GABA concentrations
  % output:
  % Fig. 3B, mean firing-rate of excitatory population of 150 brain areas at various concentrations of neurotransmitters are represented as contour plot  
  % Fig. 3F, changes in FC-distance between the empirical rs-FC and model predicted rs-FC at various concentrations of neurotransmitters are shown as contour plot
  %%
  clc; 
  clear;
  glut_conc_range = 0.1:0.1:15; % glutamate concentrations , ranging from 0.1 to 15 mmol with increment of 0.1
  gaba_conc_range = 0.1:0.1:15; % GABA concentrations, ranging from 0.1 to 15 mmol with increment of 0.1
  G_range = 0.69;               % selected global coupling factors
  %% load empirical rs-FC matrix, 150-parcellated cortex (avg. of 40 subjects)
  fcPath = load('avgFC150');
  avgFC = fcPath.rs_FC;
  fcEmp = avgFC;
  %%
  globMaxFR_E =  zeros(length(glut_conc_range),length(gaba_conc_range)); % initialization of firing rate of excitatory population
  fcDists = zeros(length(glut_conc_range),length(gaba_conc_range));      % initialization of FC distance b/w empirical rs-FC and model-predicted rs-FC
  
  for i = 1:size(G_range,2)
      G = G_range(i);
      for j = 1:size(glut_conc_range,2)
          glut_conc = glut_conc_range(j);
          for k = 1:size(gaba_conc_range,2)
              gaba_conc = gaba_conc_range(k);
                savePath = ['/../../G=',num2str(G),'_glutconc=',num2str(glut_conc),'_gabaconc=',num2str(gaba_conc),'.mat']; % add path (where simulated rs-FCs are saved)
               if exist(savePath,'file')
                load(savePath,'fcSimBold','maxfr_E');
                %fcCorrs(j,k) = find_corr(fcSimBold,fcEmp);
                fcDists(j,k) = fc_distance(fcSimBold,fcEmp,[])/150;
                globMaxFR_E(j,k) = mean(maxfr_E); 
                
              end
          end
      end
  end
 %% 
  
  %subplot(1,2,1); mean firing-rate of excitatory population of 150 brain areas as a function of concentrations of neurotransmitters
  figure();
  [gaba_conc,glut_conc] = meshgrid(gaba_conc_range,glut_conc_range);
  contourf(gaba_conc,glut_conc,globMaxFR_E);
  colorbar();
  colormap('jet');
  set(gca,'YDir','normal');
  xlabel('GABA Conc. (mmol)','FontWeight','normal','FontSize',12);
  ylabel('Glutamate Conc. (mmol)','FontWeight','normal','FontSize',12);
  title(' Mean firing-rate (150-parcellated cortex)','FontWeight','normal','FontSize',12); 
  
  
  %subplot(1,2,2); FC-distance between the empirical rs-FC and model-predicted rs-FC as a function of concentrations of neurotransmitters
  figure();
  contourf(gaba_conc_range,glut_conc_range,fcDists);
  colorbar();
  colormap('jet');
  set(gca,'YDir','normal')
  xlabel('GABA Conc. (mmol)','FontWeight','normal','FontSize',12);
  ylabel('Glutamate Conc. (mmol)','FontWeight','normal','FontSize',12);
  title('FC distance (150-parcellated cortex)','FontWeight','normal','FontSize',12);
  
  
  