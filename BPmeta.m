%Calculate filtered signal for a given HighPass, LowPass, fs(sampling freq)
%Input:
%data: NTimes * NROIs/NSensors
%HighPass: High Pass freq 
%LowPass: Low Pass freq
%fs: sampling frequency(1/TR in case of fMRI)
%Note: Consider declaring bpFilt outside the function for faster execution
function output = BPmeta(data,HighPass,LowPass,fs)
bpFilt = designfilt('bandpassfir', 'FilterOrder', 20, ...
             'CutoffFrequency1', LowPass, 'CutoffFrequency2', HighPass,...
             'SampleRate', fs);
         
output = filtfilt(bpFilt,data);
end