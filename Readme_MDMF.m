%% The codes in this folder generates the figures used in the manuscript Naskar, A, Vattikonda, A., Deco, G. , Roy. D. and Banerjee, A (under review)
% Multi-scale variant of Dynamic field model (MDMF)
% The codes simulate synaptic activity using MDMF equations and generate rs-FCs via computing BOLD signal (Friston - Ballon MODEL)
%% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
%% To plot figures Fig2A-B - Steady state approximations of synaptic gating dynamics
% SourceAveGating.m
% The following functions are need to be in the same path, gen_spikes.m, sim_gating_var.m
%%--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%% plot Fig.2C,D,E - FC distance, average firing rate of excitatory and inhibitory population as a function of global coupling strength (G)
% prerequisite: 
% generate resting-state functional connectivity (rs-FC) matrices, firing rate of excitatory population and inhibitory population, at various G  
% with fixed concentration of GABA and glutamate, solved the system of equations (3)−(9) numerically  
% input: 
% avgSC68 = structural connectivity (SC) matrix, average of SC matrices obtained from 40 subjects (data collected from Cambridge Centre for Ageing, and Neuroscience)
% avgFC68 = empirical rs-FC, average of rs-FC matrices obtained from 40 subjects (data collected from Cambridge Centre for Ageing, and Neuroscience)
% Source_VaryGalphaBetaSupport.m
% output: rs-FCs, firing rate of excitatory population and inhibitory population 
% The following functions are need to be in the same path, VaryGalphan_beta.m, phie.m, phii.m, BOLD.m 
% Fig.2C,D,E 
% input: rs-FCs, firing rate of excitatory population and inhibitory population at various G 
% Source_varyG_MDMFresult.m
% The following function is need to be in the same path, fc_distance.m
% Fig.2F & 2G, Firing rate of excitatory and inhibitory populations of all 68 brain areas at G=0.69, respectively
% Source_varyG_MDMFresult.m
%%-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%% To plot Fig.3A & 3E, neuronal firing, and FC distance as a function of GABA-glutamate concentrations, considering 68-parcellated cortex
% prerequisite: 
% simulate synaptic activity using MDMF equations and generate rs-FCs at various GABA-glutamate concentrations 
% input: avgSC68 = structural connectivity (SC)
% Source_MDMF68.m
% The following functions are need to be in the same path, MDMFalphan_beta68.m, phie.m, phii.m, BOLD.m 
% output: model generated rs-FCs at various concentration of GABA-glutamate
% Fig.3A & 3E
% input: avgFC68 = functional connectivity (FC), model generated rs-FCs at various concentrations of GABA-glutamate
% Source_MDMF_results68.m
% The following function is need to be in the same path, fc_distance.m 
%%--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%% To generate Fig.3B & 3F, neuronal firing, and FC distance as a function of GABA-glutamate concentrations, considering 150-parcellated cortex
% prerequisite: 
% generate rs-FCs via simulating synaptic activity at various GABA-glutamate concentrations 
% input: avgSC150 = SC
% Source_MDMF150.m
% The following functions are need to be in the same path, MDMFalphan_beta150.m, phie.m, phii.m, BOLD.m 
% output: model generated rs-FCs at various concentrations of GABA-glutamate
% Fig.3B & 3F 
% input: avgFC150 = FC, model generated rs-FCs at various concentrations of GABA-glutamate
% Source_MDMF_results150.m
% The following function is need to be in the same path, fc_distance.m 
%%--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%% plot Fig.3C, metastability as a function of GABA-glutamate concentrations (considering 68-parcellated cortex)
% prerequisite: 
% generate simulated-BOLD time series at various concentrations of GABA-glutamate
% input: avgSC68 = SC
% Source_MDMF_timeSeries68.m
% The following functions are need to be in the same path, MDMFalphan_beta68ts.m, phie.m, phii.m, BOLD.m  
% output: simulated time-series
% Band-pass filter of model generated-BOLD signal 
% input: simulated time-series
% SourceBandPassFilteredTimeSeries68or150.m
% The following function is need to be in the same path, BPmeta.m
%  output: band-pass filtered BOLD signal 
% plot Fig.3C
% input: band-pass filtered BOLD signal
% Source_metastability_compute_results68.m
% The following function is need to be in the same path, MetaStab.m
%%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%% plot Fig.3D, metastability as a function of GABA-glutamate concentrations (considering 150-parcellated cortex)
% prerequisite: 
% generate simulated-BOLD time series at various concentrations of GABA-glutamate
% input: avgSC150 = SC
% Source_MDMF_timeSeries150.m
% The following functions are need to be in the same path, MDMFalphan_beta150ts.m, phie.m, phii.m, BOLD.m  
% output: simulated time-series
% Band-pass filter of model generated-BOLD signal 
% input: simulated time-series
% SourceBandPassFilteredTimeSeries68or150.m
% The following function is need to be in the same path, BPmeta.m
% output: band-pass filtered BOLD signal 
% plot Fig.3D
% input: band-pass filtered BOLD signal
% Source_metastability_compute_results150.m
% The following function is need to be in the same path, MetaStab.m
%%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%% Network construction 
% model generated rs-FCs convert to absolute matrices 
% thresholded with various values 
% binarize matrices, and compute graph-theoretic measures 
% use functions from Brain Connectivity Toolbox (BCT)
% To compute graph theoretical measures such as clustering coefficient, characteristic path length, global efficiency, and modularity  
% across various concentrations of GABA-glutamate at various threshold values
% graph_metrics_FCThrbin.m
%%----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
% plot Fig.4A, Fig.4B, changes in modularity and clustering coefficient of simulated connectivity matrices as a function of GABA-glutamate concentrations
% plot Fig.4C, Fig.4D, changes in modularity and clustering coefficient at various GABA concentrations with discrete values of glutamate concentrations 
% input: connectivity matrices thresholded with 0.25 (following network construction) = graph_measures_thr=0.25_bin_.mat
% use functions from BCT Toolbox
% Source_graphTheoAnaly_plotFig4.m
%%----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
% plot Fig.5A, Fig.5B, changes in characteristic path length and global efficiency of simulated connectivity matrices as a function of GABA-glutamate concentrations
% plot Fig.5C, Fig.5D, changes in characteristic path length and global efficiency at various GABA concentrations with discrete values of glutamate concentrations 
% input: connectivity matrices thresholded with 0.25 (following network construction) = graph_measures_thr=0.25_bin_.mat
% use functions from BCT Toolbox
% Source_graphTheoAnaly_plotFig5.m
%%----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%% plot Fig.6A, Fig.6B, Fig.6C, Fig.6D, Fig.6E & Fig.6F, comparison of empirical rs-FC and simulated rs-FCs (including default mode network)
% input: 
% G=0.69_glutconc=12_gabaconc=1.5.mat = simulated FC (glutamate: 12 mmol & GABA: 1.5 mmol) 
% G=0.69_glutconc=7_gabaconc=1.5.mat = simulated FC (glutamate: 7 mmol & GABA: 1.5 mmol) 
% G=0.69_glutconc=3_gabaconc=1.5.mat = simulated FC (glutamate: 3 mmol & GABA: 1.5 mmol)  
% glt12gaba1point5.mat = simulated DMN (glutamate: 12 mmol & GABA: 1.5 mmol) 
% glt7gaba1point5.mat = simulated DMN (glutamate: 7 mmol & GABA: 1.5 mmol) 
% glt3gaba1point5.mat = simulated DMN (glutamate: 3 mmol & GABA: 1.5 mmol) 
% Source_DMN_fcd_results.m
%%----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------













